---
dates:
  start: "2020-08"
  end:
name: "Weavik"
location: "Waterloo, ON"
logo: "./logos/weavik.png"
position: "Full Stack Developer"
published: true
skills: ["nestjs", "react", "react-native", "docker", "github", "postgresql"]
slug: "focus21"
types: ["full-time"]
website: "https://www.weavik.com"
---

- Write engaging and maintainable React Native apps in a small team environment.
- Create and implement development processes for React Native apps
- _More to follow in months to come!_
