---
date: 2020-08-31
image: ./images/payme.png
name: PayMe
link: https://www.gitlab.com/kendallroth/payme
pinned: true
published: true
slug: payme
status: Active
tags: [react-native, android]
type: App
---

**PayMe** is a simple Android app to track who has paid for an event. I was tired of constantly having to create lists (in various apps or on paper) of who had been at what event, and whether they had paid! Since I was also learning React Native at the time for [Weavik](https://www.weavik.com), I decided to experiment with creating an app!

> Track people's payments across events!

The app provides a simple way to manage a list of people and events, joining the two via event attendance. People can be checked off the attendance list as they pay, allowing the organizer to get a clear picture of who has paid an event. A helpful dashboard ensures that the highest unpaid events and people can easily be noted for follow up.

The project is built with React Native for Android, following Google's Material Design concepts. It involves several new concepts for me, including React Native itself, Redux Toolkit, flattened state management, and integration with an app store. The app is usable and being internally tested, although there are some more features/fixes that are necessary before a public release.
